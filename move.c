#include "board.h"

char *
print_square(Board_Position bp, char *s)
{

	*(s++) = 'a' + (bp % 10 - 1);
	*(s++) = '1' + (bp / 10 - 2);
	*s = '\0';
	return s;
}

char *
print_move(Move m, char *s)
{
	Board_Position from = FROM_SQ(m);
	Board_Position to = TO_SQ(m);

	s = print_square(from, s);
	s = print_square(to, s);

	Piece promoted = PROMOTED(m);
	if (promoted) {
		if (promoted & QUEEN_FLAG)
			*(s++) = 'q';
		if (promoted & ROOK_FLAG)
			*(s++) = 'r';
		if (promoted & BISHOP_FLAG)
			*(s++) = 'b';
		if (promoted & KNIGHT_FLAG)
			*(s++) = 'n';
	}

	*s = '\0';

	return s;
}
