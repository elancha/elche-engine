CFLAGS +=-ggdb -Wall -Wextra -std=c2x -pedantic
CFLAGS += -DDEBUG
LDFLAGS +=

.PHONY: all clean

all: elche_engine

elche_engine: elche_engine.c board.c board.h move.c assert.h movegen.c
	cc $(CFLAGS) -o elche_engine elche_engine.c board.c move.c movegen.c $(LDFLAGS)

clean:
	rm -rf elche_engine
