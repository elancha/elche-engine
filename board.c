#include <stdio.h>
#include <stdlib.h>

#include "assert.h"
#include "board.h"

uint64_t hash_piece_keys[13][120];
uint64_t hash_side_key;
uint64_t hash_castle_keys[14];


uint64_t
random_64(void)
{
	/* TODO: Update to GCC 14 and use a pseudorandom generator */
	return ((uint64_t)rand() <<  0) |
	       ((uint64_t)rand() << 15) |
	       ((uint64_t)rand() << 30) |
	       ((uint64_t)rand() << 45) |
	       (((uint64_t)rand() & 0xf) << 60);
}

void
init_hash_keys(void)
{
	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 120; j++) {
			hash_piece_keys[i][j] = random_64();
		}
	}
	hash_side_key = random_64();
	for (int i = 0; i < 16; i++) {
		hash_castle_keys[i] = random_64();
	}
}

uint64_t
generate_position_hash(const Board *b)
{
	uint64_t final_key = 0;

	for (int i = 0; i < 120; i++) {
		Piece piece = b->pieces[i];
		if (piece != OFFBOARD) {
			final_key ^= hash_piece_keys[CPIECE_NUMBER(piece)][i];
		}
	}

	if (b->side == WHITE) {
		final_key ^= hash_side_key;
	}

	if (b->en_passant != NO_SQ) {
		final_key ^= hash_piece_keys[CPIECE_NUMBER(EMPTY)][b->en_passant];
	}

	final_key ^= hash_castle_keys[b->castle_perimission];

	return final_key;
}

void
reset_board(Board *b)
{
	for (int i = 0; i < 120; i++) {
		b->pieces[i] = OFFBOARD;
	}

	for (int i = 0; i < 64; i++) {
		int index = SQ120((i % 8), (i / 8));
		b->pieces[index] = EMPTY;
	}

	for (int i = 0; i < 3; i++) {
		b->pawns[i] = 0ULL;
		b->big_pieces[i] = 0;
		b->major_pieces[i] = 0;
		b->minor_pieces[i] = 0;
		b->material[i] = 0;
	}

	b->king_square[0] = NO_SQ,
	b->king_square[1] = NO_SQ;

	b->side = BOTH;
	b->en_passant = NO_SQ;
	b->fifty_move_counter = 0;

	b->ply = 0;
	b->history_ply = 0;
	b->castle_perimission = 0;
	b->position_hash = 0;

	for (int i = 0; i < MAX_GAME_MOVES; i++) {
		b->history[i] = (Undo){0};
	}
}

int
parse_fen(const char *fen, Board  *b)
{
	reset_board(b);

	Rank current_rank = RANK_8;
	File current_file = FILE_A;

	while ((current_rank >= RANK_1) && *fen) {
		Piece piece;
		/* number of pieces to set */
		int count = 1;
		switch (*fen) {
		case 'p':
			piece = BLACK_PAWN;
			break;
		case 'r':
			piece = BLACK_ROOK;
			break;
		case 'n':
			piece = BLACK_KNIGHT;
			break;
		case 'b':
			piece = BLACK_BISHOP;
			break;
		case 'q':
			piece = BLACK_QUEEN;
			break;
		case 'k':
			piece = BLACK_KING;
			break;
		case 'P':
			piece = WHITE_PAWN;
			break;
		case 'R':
			piece = WHITE_ROOK;
			break;
		case 'N':
			piece = WHITE_KNIGHT;
			break;
		case 'B':
			piece = WHITE_BISHOP;
			break;
		case 'Q':
			piece = WHITE_QUEEN;
			break;
		case 'K':
			piece = WHITE_KING;
			break;

		case '1': [[fallthrough]];
		case '2': [[fallthrough]];
		case '3': [[fallthrough]];
		case '4': [[fallthrough]];
		case '5': [[fallthrough]];
		case '6': [[fallthrough]];
		case '7': [[fallthrough]];
		case '8':
			piece = EMPTY;
			count = *fen - '0';
			break;

		case '/': [[fallthrough]];
		case ' ':
			current_rank--;
			current_file = FILE_A;
			count = 0; /* do not set any pieces */
			break;
		default:
			printf("FEN error\n");
			return -1;
		}

		for (int i = 0; i < count; i++) {
			b->pieces[SQ120(current_file, current_rank)] = piece;
			current_file++;
		}
		fen++;
	}

	if (*fen != 'w' && *fen != 'b') {
		printf("FEN error\n");
		return -1;
	}
	b->side = (*fen == 'w') ? WHITE : BLACK;
	fen += 2;

	for (int i = 0; i < 4; i++) {
		if (*fen == ' ')
			break;

		switch (*fen) {
		case 'K':
			b->castle_perimission |= WHITE_KING_CASTLE_ALLOWED;
			break;
		case 'Q':
			b->castle_perimission |= WHITE_QUEEN_CASTLE_ALLOWED;
			break;
		case 'k':
			b->castle_perimission |= BLACK_KING_CASTLE_ALLOWED;
			break;
		case 'q':
			b->castle_perimission |= BLACK_QUEEN_CASTLE_ALLOWED;
			break;
		default:
			break;
		}

		fen++;
	}
	fen++;

	if (*fen != '-') {
		current_file = fen[0] - 'a';
		current_rank = fen[1] - '1';

		if (current_file < FILE_A || current_file > FILE_H) {
			printf("FEN error\n");
			return -1;
		}
		if (current_rank < RANK_1|| current_rank > RANK_8) {
			printf("FEN error\n");
			return -1;
		}
		b->en_passant = SQ120(current_file, current_rank);
	}

	/* TODO: halfmoves and fullmoves */

	b->position_hash = generate_position_hash(b);

	update_list_material(b);

	return 0;
}
/* TODO: find a place for this */
const int piece_value[7] = {0, 100, 325, 325, 550, 1000, 50000};

int
check_board(const Board *b)
{
	int t_piece_number[13] = {0, 0};
	int t_big_pieces[2] = {0, 0};
	int t_minor_pieces[2] = {0, 0};
	int t_major_pieces[2] = {0, 0};
	int t_material[2] = {0, 0};

	BitBoard t_pawns[3];

	t_pawns[WHITE] = b->pawns[WHITE];
	t_pawns[BLACK] = b->pawns[BLACK];
	t_pawns[BOTH] = b->pawns[BOTH];

	for (int c = 0; c < 2; c++) {
		for (int i = 1; i <= 6; i++) {
			Piece p = (c << 7) | (1 << i);
			(void) p;
			for (int j = 0; j < b->piece_number[6*c + i]; j++) {
				Board_Position sq = b->piece_list[6*c + i][j];
				(void)sq;
				ASSERT(b->pieces[sq] == p);
			}
		}
	}

	for (int i = 0; i < 64; i++) {
		int sq = SQ120((i%8), (i/8));
		Piece p = b->pieces[sq];

		t_piece_number[CPIECE_NUMBER(p)]++;

		if (p & BIG_FLAG)
			t_big_pieces[COLOUR(p)]++;
		if (p & MINOR_FLAG)
			t_minor_pieces[COLOUR(p)]++;
		if (p & MAJOR_FLAG)
			t_major_pieces[COLOUR(p)]++;

		t_material[COLOUR(p)] += piece_value[PIECE_NUMBER(p)];
	}

	for (int i = 1; i < 13; i++) {
		ASSERT(t_piece_number[i] == b->piece_number[i]);
	}

	ASSERT(count_bits(t_pawns[WHITE]) == b->piece_number[CPIECE_NUMBER(WHITE_PAWN)]);
	ASSERT(count_bits(t_pawns[BLACK]) == b->piece_number[CPIECE_NUMBER(BLACK_PAWN)]);
	ASSERT(count_bits(t_pawns[BOTH]) == (b->piece_number[CPIECE_NUMBER(WHITE_PAWN)] + b->piece_number[CPIECE_NUMBER(BLACK_PAWN)]));

	while (t_pawns[WHITE]) {
		int sq64 = pop_bit(&t_pawns[WHITE]);
		int idx = SQ120((sq64 % 8), (sq64 / 8));
		(void) idx;
		ASSERT(b->pieces[idx] == WHITE_PAWN);
	}
	while (t_pawns[BLACK]) {
		int sq64 = pop_bit(&t_pawns[BLACK]);
		int idx = SQ120((sq64 % 8), (sq64 / 8));
		(void) idx;
		ASSERT(b->pieces[idx] == BLACK_PAWN);
	}
	while (t_pawns[BOTH]) {
		int sq64 = pop_bit(&t_pawns[BOTH]);
		int idx = SQ120((sq64 % 8), (sq64 / 8));
		(void) idx;
		ASSERT(b->pieces[idx] & PAWN_FLAG);
	}

	ASSERT(t_material[WHITE] == b->material[WHITE] && t_material[BLACK] == b->material[BLACK]);
	ASSERT(t_minor_pieces[WHITE] == b->minor_pieces[WHITE] && t_minor_pieces[BLACK] == b->minor_pieces[BLACK]);
	ASSERT(t_major_pieces[WHITE] == b->major_pieces[WHITE] && t_major_pieces[BLACK] == b->major_pieces[BLACK]);
	ASSERT(t_big_pieces[WHITE] == b->big_pieces[WHITE] && t_big_pieces[BLACK] == b->big_pieces[BLACK]);

	ASSERT(b->side == WHITE || b->side == BLACK);
	ASSERT(generate_position_hash(b) == b->position_hash);

	ASSERT(b->en_passant == NO_SQ ||
	    (b->side == WHITE && (b->en_passant / 10 - 2) == RANK_6) ||
	    (b->side == BLACK && (b->en_passant / 10 - 2) == RANK_3));

	ASSERT(b->pieces[b->king_square[WHITE]] == WHITE_KING);
	ASSERT(b->pieces[b->king_square[BLACK]] == BLACK_KING);

	return 1;
}

void
update_list_material(Board *b)
{
	for (int i = 0; i < 120; i++) {
		Piece p = b->pieces[i];
		if (p != OFFBOARD && p != EMPTY) {

			if (p & BIG_FLAG)
				b->big_pieces[COLOUR(p)]++;
			if (p & MINOR_FLAG)
				b->minor_pieces[COLOUR(p)]++;
			if (p & MAJOR_FLAG)
				b->major_pieces[COLOUR(p)]++;

			b->material[COLOUR(p)] += piece_value[PIECE_NUMBER(p)];

			/* Piece list */
			b->piece_list[CPIECE_NUMBER(p)][b->piece_number[CPIECE_NUMBER(p)]] = i;
			b->piece_number[CPIECE_NUMBER(p)]++;

			if (p == WHITE_KING)
				b->king_square[WHITE] = i;
			if (p == BLACK_KING)
				b->king_square[BLACK] = i;

			if (p & PAWN_FLAG) {
				b->pawns[COLOUR(p)] |= (1ULL << SQ64((i % 10) - 1, (i / 10) - 2));
				b->pawns[BOTH]      |= (1ULL << SQ64((i % 10) - 1, (i / 10) - 2));
			}
		}
	}
}

const int knight_direction[8] = {-8, -19, -21, -12, 8, 19, 21, 12};
const int rook_direction[4] = {-1, -10, 1, 10};
const int bishop_direction[4] = {-9, -11, 11, 9};
const int king_direction[8] = {-1, -10, 1, 10, -9, -11, 11, 9};

int
is_square_atakked(const Board *b, Board_Position sq, Side side)
{
	int rr = sq % 10;
	(void) rr;
	ASSERT(rr != 0 && rr != 9);
	ASSERT(sq > 20 && sq < 99);
	ASSERT(b->pieces[sq] != OFFBOARD);
	ASSERT(side == WHITE || side == BLACK);
	ASSERT(check_board(b));

	/* PAWNS */
	if (side == WHITE) {
		if (b->pieces[sq-11] == WHITE_PAWN || b->pieces[sq-9] == WHITE_PAWN)
			return 1;
	} else {
		if (b->pieces[sq+11] == WHITE_PAWN || b->pieces[sq+9] == WHITE_PAWN)
			return 1;
	}

	/* KNIGHTS */
	for (int i = 0; i < 8; i++) {
		Piece p = b->pieces[sq + knight_direction[i]];
		if ((p & KNIGHT_FLAG) && COLOUR(p) == side)
			return 1;
	}

	/* KINGS */
	for (int i = 0; i < 8; i++) {
		Piece p = b->pieces[sq + king_direction[i]];
		if ((p & KING_FLAG) && COLOUR(p) == side)
			return 1;
	}

	/* ROOKS, QUEENS */
	for (int i = 0; i < 4; i++) {
		int direction = rook_direction[i];
		Board_Position t_sq = sq + direction;
		Piece p = b->pieces[t_sq];
		while (p != OFFBOARD) {
			if (p & HAS_PIECE_FLAG) {
				if ((p & (ROOK_FLAG|QUEEN_FLAG)) && COLOUR(p) == side)
					return 1;
				break;
			}
			t_sq += direction;
			p = b->pieces[t_sq];
		}
	}

	/* BISHOP, QUEENS */
	for (int i = 0; i < 4; i++) {
		int direction = bishop_direction[i];
		Board_Position t_sq = sq + direction;
		Piece p = b->pieces[t_sq];
		while (p != OFFBOARD) {
			if (p & HAS_PIECE_FLAG) {
				if ((p & (BISHOP_FLAG|QUEEN_FLAG)) && COLOUR(p) == side)
					return 1;
				break;
			}
			t_sq += direction;
			p = b->pieces[t_sq];
		}
	}

	return 0;
}

static const char piece_char[] = ".PNBRQKpnbrqk";
static const char side_char[] = "wb-";
static const char rank_char[] = "12345678";
static const char file_char[] = "abcdefgh";

void
print_board(const Board *b)
{
	for (Rank rank = RANK_8; rank >= RANK_1; rank--) {
		printf("%c  ", rank_char[rank]);
		for (File file = FILE_A; file <= FILE_H; file++) {
			Piece p = b->pieces[SQ120(file, rank)];
			printf("%3c", piece_char[CPIECE_NUMBER(p)]);
		}
		printf("\n");
	}
	printf("\n   ");
	for (File file = FILE_A; file <= FILE_H; file++) {
		printf("%3c", file_char[file]);
	}
	printf("\n");

	printf("side: %c\n", side_char[b->side]);
	printf("en_passant: %d\n", b->en_passant);
	printf("castle: %c%c%c%c\n", b->castle_perimission & WHITE_KING_CASTLE_ALLOWED ? 'K' : '-',
	                             b->castle_perimission & WHITE_QUEEN_CASTLE_ALLOWED ? 'Q' : '-',
	                             b->castle_perimission & BLACK_KING_CASTLE_ALLOWED ? 'k' : '-',
	                             b->castle_perimission & BLACK_QUEEN_CASTLE_ALLOWED ? 'q' : '-');
	printf("position_hash: %lx\n", b->position_hash);
}

void
print_bitboard(BitBoard bb)
{
	for (Rank rank = RANK_8; rank >= RANK_1; rank--) {
		for (File file = FILE_A; file <= FILE_H; file++) {
			int idx = (rank * 8) + file;
			printf((1ULL << idx) & bb ? "X" : "-");
		}
		printf("\n");
	}
	printf("\n");
}

int
pop_bit(BitBoard *bb) {
	int r = __builtin_ctzll(*bb);
	*bb &= (*bb - 1);
	return r;
}

int
count_bits(BitBoard bb)
{
#ifdef ASM_POPCNT
	int count;
	__asm("popcnt %q1, %q0" : "=r"(count) : "rm"(bb) : "cc");
	return count;
#else 
	return __builtin_popcountll(bb);
#endif
}


