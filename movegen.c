#include "assert.h"
#include "board.h"

#define MOVE(from, to, captured, promoted, flags) \
	( (from) | ((to) << 7) | (((captured) &0xfe) << 16) | (((promoted) & 0xfe) << 24) | (flags) )

void
add_scored_move(const Board *b, Move_List *list, Move m, Score s)
{
	(void) b;
	list->scored_moves[list->count].move = m;
	list->scored_moves[list->count].score = s;
	list->count++;
}

constexpr int non_sliding_piece_dir[2][8] = {
	{-8, -19, -21, -12,  8,  19, 21, 12}, /* KNIGTH */
	{-1, -10,   1,  10, -9, -11,  9, 11}, /* KING */
};

void
generate_all_moves(const Board *b, Move_List *list)
{
	ASSERT(check_board(b));
	list->count = 0;

	if (b->side == WHITE) {
		for (int i = 0; i < b->piece_number[CPIECE_NUMBER(WHITE_PAWN)]; i++) {
			Board_Position sq = b->piece_list[CPIECE_NUMBER(WHITE_PAWN)][i];

			/* ASSERT sq on board */
			ASSERT(b->pieces[sq] != OFFBOARD);
			ASSERT(sq > 20 && sq < 99);
			int idx = sq  % 10;
			(void) idx;
			ASSERT(idx > 0 && idx < 9);

			/* Pawn forward move */
			if (b->pieces[sq + 10] == EMPTY) {
				if (RANK(sq) == RANK_7) {
					Move m = MOVE(sq, sq+10, EMPTY, WHITE_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+10, EMPTY, WHITE_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+10, EMPTY, WHITE_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+10, EMPTY, WHITE_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq+10, EMPTY, EMPTY, 0);
					add_scored_move(b, list, m, 0);

					if (RANK(sq) == RANK_2 && b->pieces[sq+20] == EMPTY) {
						Move m = MOVE(sq, sq+20, EMPTY, EMPTY, PAWN_START);
						add_scored_move(b, list, m, 0);
					}
				}
			}

			/* Pawn captures */
			if ((b->pieces[sq + 9] & HAS_PIECE_FLAG) && (b->pieces[sq + 9] & BLACK_FLAG)) {
				if (RANK(sq) == RANK_7) {
					Move m = MOVE(sq, sq+9, b->pieces[sq+9], WHITE_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+9, b->pieces[sq+9], WHITE_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+9, b->pieces[sq+9], WHITE_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+9, b->pieces[sq+9], WHITE_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq+9, b->pieces[sq+9], EMPTY, 0);
					add_scored_move(b, list, m, 0);
				}
			}
			if ((b->pieces[sq + 11] & HAS_PIECE_FLAG) && (b->pieces[sq + 11] & BLACK_FLAG)) {
				if (RANK(sq) == RANK_7) {
					Move m = MOVE(sq, sq+11, b->pieces[sq+11], WHITE_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+11, b->pieces[sq+11], WHITE_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+11, b->pieces[sq+11], WHITE_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq+11, b->pieces[sq+11], WHITE_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq+11, b->pieces[sq+11], EMPTY, 0);
					add_scored_move(b, list, m, 0);
				}
			}

			/* Pawn en passant */
			if (sq + 9 == b->en_passant) {
				Move m = MOVE(sq, sq+9, EMPTY, EMPTY, EN_PASSANT);
				add_scored_move(b, list, m, 0);
			}
			if (sq + 11 == b->en_passant) {
				Move m = MOVE(sq, sq+11, EMPTY, EMPTY, EN_PASSANT);
				add_scored_move(b, list, m, 0);
			}
		}
	} else {
		for (int i = 0; i < b->piece_number[CPIECE_NUMBER(BLACK_PAWN)]; i++) {
			Board_Position sq = b->piece_list[CPIECE_NUMBER(BLACK_PAWN)][i];

			/* ASSERT sq on board */
			ASSERT(b->pieces[sq] != OFFBOARD);
			ASSERT(sq > 20 && sq < 99);
			int idx = sq  % 10;
			(void) idx;
			ASSERT(idx > 0 && idx < 9);

			/* Pawn forward move */
			if (b->pieces[sq - 10] == EMPTY) {
				if (RANK(sq) == RANK_2) {
					Move m = MOVE(sq, sq-10, EMPTY, BLACK_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-10, EMPTY, BLACK_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-10, EMPTY, BLACK_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-10, EMPTY, BLACK_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq-10, EMPTY, EMPTY, 0);
					add_scored_move(b, list, m, 0);

					if (RANK(sq) == RANK_7 && b->pieces[sq-20] == EMPTY) {
						Move m = MOVE(sq, sq-20, EMPTY, EMPTY, PAWN_START);
						add_scored_move(b, list, m, 0);
					}
				}
			}

			/* Pawn captures */
			if ((b->pieces[sq - 9] & HAS_PIECE_FLAG) && !(b->pieces[sq - 9] & BLACK_FLAG)) {
				if (RANK(sq) == RANK_2) {
					Move m = MOVE(sq, sq-9, b->pieces[sq-9], BLACK_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-9, b->pieces[sq-9], BLACK_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-9, b->pieces[sq-9], BLACK_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-9, b->pieces[sq-9], BLACK_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq-9, b->pieces[sq-9], EMPTY, 0);
					add_scored_move(b, list, m, 0);
				}
			}
			if ((b->pieces[sq - 11] & HAS_PIECE_FLAG) && !(b->pieces[sq - 11] & BLACK_FLAG)) {
				if (RANK(sq) == RANK_2) {
					Move m = MOVE(sq, sq-11, b->pieces[sq-11], BLACK_QUEEN, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-11, b->pieces[sq-11], BLACK_ROOK, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-11, b->pieces[sq-11], BLACK_BISHOP, 0);
					add_scored_move(b, list, m, 0);
					m = MOVE(sq, sq-11, b->pieces[sq-11], BLACK_KNIGHT, 0);
					add_scored_move(b, list, m, 0);
				} else {
					Move m = MOVE(sq, sq-11, b->pieces[sq-11], EMPTY, 0);
					add_scored_move(b, list, m, 0);
				}
			}

			/* Pawn en passant */
			if (sq - 9 == b->en_passant) {
				Move m = MOVE(sq, sq-9, EMPTY, EMPTY, EN_PASSANT);
				add_scored_move(b, list, m, 0);
			}
			if (sq - 11 == b->en_passant) {
				Move m = MOVE(sq, sq-11, EMPTY, EMPTY, EN_PASSANT);
				add_scored_move(b, list, m, 0);
			}
		}
	}

	/* Sliding pieces */
	for (int i = 0; i < 3; i++) {
		Piece p = (0x10 << i) | (b->side << 7);
		printf("Piece sliding p: %x\n", p);
	}

	/* Non sliding pieces */
	for (int i = 0; i < 2; i++) {
		/* TODO: optimize this */
		Piece p = (0x04 << i) | (b->side << 7);
		printf("Piece non-sliding p: %x\n", p);
		for (int j = 0; j < b->piece_number[CPIECE_NUMBER(p)]; j++) {
			Board_Position sq = b->piece_list[CPIECE_NUMBER(p)][j];

			char sqbuf[3];
			print_square(sq, sqbuf);
			printf("Piece on %s\n", sqbuf);

			/* ASSERT sq on board */
			ASSERT(b->pieces[sq] != OFFBOARD);
			ASSERT(sq > 20 && sq < 99);
			int idx = sq  % 10;
			(void) idx;
			ASSERT(idx > 0 && idx < 9);

			for (int k = 0; k < 8; k++) {
				Board_Position t_sq = sq + non_sliding_piece_dir[i][k];

				if (b->pieces[t_sq] == OFFBOARD)
					continue;

				if (b->pieces[t_sq] & HAS_PIECE_FLAG) {
					if (COLOUR(b->pieces[t_sq]) != b->side) {
						print_square(t_sq, sqbuf);
						printf("\tcapture on %s\n", sqbuf);
					}
				} else {
					print_square(t_sq, sqbuf);
					printf("\tNormal move on %s\n", sqbuf);
				}
			}
		}
	}

}

#include <stdio.h>

void
print_move_list(const Move_List *list)
{
	printf("Move list:\n");
	for (int i = 0; i < list->count; i++) {
		Move move = list->scored_moves[i].move;
		int score = list->scored_moves[i].score;

		char move_buf[6];
		print_square(move, move_buf);
		printf("Move: %d > %s (score %d)\n", i+1, move_buf, score);
	}
	printf("Move List Total %d Moves\n\n", list->count);
}
