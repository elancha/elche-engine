#include <stdio.h>

#include "assert.h"
#include "board.h"

#define FEN1 "8/3q1p2/8/5P2/4Q3/8/8/8 w HAha - 0 1"
#define PAWNMOVESW "rnbqkb1r/pp1p1pPp/8/2p1pP2/1P1P4/3P3P/P1P1P3/RNBQKBNR w KQkq e6 0 1"
#define PAWNMOVESB "rnbqkbnr/p1p1p3/3p3p/1p1p4/2P1Pp2/8/PP1P1PpP/RNBQKB1R b KQkq e3 0 1"
#define KNIGHTSKINGS "5k2/1n6/4n3/6N1/8/3N4/8/5K2 b HAha - 0 1"

void foo(Side side, Board *b)
{
	printf("\n\nSquares attacked by %s\n", side == WHITE ? "WHITE" : "BLACK");
	for (Rank rank = RANK_8; rank >= RANK_1; --rank) {
		for (File file = FILE_A; file <= FILE_H; ++file) {
			Board_Position sq = SQ120(file, rank);
			printf("%c", is_square_atakked(b, sq, side) ? 'X' : '-');
		}
		printf("\n");
	}
	printf("\n\n");
}

int
main()
{
	init_hash_keys();

	/* TODO: make board 0 initializable */
	Board board;
	parse_fen(KNIGHTSKINGS, &board);
	print_board(&board);

	Move_List list;
	generate_all_moves(&board, &list);

	print_move_list(&list);

	return 0;
}
