#include <inttypes.h>

/* Should be more than enough */
#define MAX_GAME_MOVES 2048
#define MAX_POSITION_MOVES 256

#define START_FEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"


/* TODO: revisit this */
#define HAS_PIECE_FLAG 0x7E

#define COLOUR(p) (((p) & 0x80) >> 7)
#define BLACK_FLAG 0x80

#define BIG_FLAG   (KNIGHT_FLAG|BISHOP_FLAG|ROOK_FLAG|QUEEN_FLAG)
#define MAJOR_FLAG (ROOK_FLAG|QUEEN_FLAG)
#define MINOR_FLAG (BISHOP_FLAG|KNIGHT_FLAG)

#define PAWN_FLAG   0x02
#define KNIGHT_FLAG 0x04
#define KING_FLAG   0x08
#define BISHOP_FLAG 0x10
#define ROOK_FLAG   0x20
#define QUEEN_FLAG  0x40

#define PIECE_NUMBER(p) __builtin_ctzl(p)
#define CPIECE_NUMBER(p) ( ((((p) & 0x80) >> 5) | (((p) & 0x80) >> 6)) + PIECE_NUMBER(p))

/* TODO: maybe en_passant is needed */
typedef enum : uint8_t {
	OFFBOARD = 0x00,
	EMPTY = 0x01,

	WHITE_PAWN   = 0x02,
	WHITE_KNIGHT = 0x04,
	WHITE_KING   = 0x08,
	WHITE_BISHOP = 0x10,
	WHITE_ROOK   = 0x20,
	WHITE_QUEEN  = 0x40,

	BLACK_PAWN   = 0x82,
	BLACK_KNIGHT = 0x84,
	BLACK_KING   = 0x88,
	BLACK_BISHOP = 0x90,
	BLACK_ROOK   = 0xA0,
	BLACK_QUEEN  = 0xC0,
} Piece;

typedef enum {
	WHITE,
	BLACK,
	BOTH,
} Side;

typedef enum {
	WHITE_KING_CASTLE_ALLOWED  = 1,
	WHITE_QUEEN_CASTLE_ALLOWED = 2,
	BLACK_KING_CASTLE_ALLOWED  = 4,
	BLACK_QUEEN_CASTLE_ALLOWED = 8,
} Castle_Permission;

typedef enum : int {
	RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8,
} Rank;

typedef enum : int {
	FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H,
} File;

typedef enum : int {
	NO_SQ = 0,
	A1 = 21, B1, C1, D1, E1, F1, G1, H1,
	A2 = 31, B2, C2, D2, E2, F2, G2, H2,
	A3 = 41, B3, C3, D3, E3, F3, G3, H3,
	A4 = 51, B4, C4, D4, E4, F4, G4, H4,
	A5 = 61, B5, C5, D5, E5, F5, G5, H5,
	A6 = 71, B6, C6, D6, E6, F6, G6, H6,
	A7 = 81, B7, C7, D7, E7, F7, G7, H7,
	A8 = 91, B8, C8, D8, E8, F8, G8, H8,
} Board_Position;

#define SQ120(file, rank) (((rank)+2)*10 + (file) + 1)
#define SQ64(file, rank) ((rank)*8  + (file))

#define RANK(x) (((x) / 10) - 2)
#define FILE(x) (((x) % 10) - 1)


typedef uint64_t BitBoard;

/* move structure */
/*
 *  0000 0000 0000 0000 0000 0000 0111 1111 -> From
 *  0000 0000 0000 0000 0011 1111 1000 0000 -> To
 *  0000 0000 0000 0000 0100 0000 0000 0000 -> Pawn start
 *  0000 0000 0000 0000 1000 0000 0000 0000 -> En passant
 *  0000 0000 0000 0001 0000 0000 0000 0000 -> Castle
 *  0000 0000 1111 1110 0000 0000 0000 0000 -> Captured
 *  1111 1110 0000 0000 0000 0000 0000 0000 -> Promoted to
 */

#define PAWN_START 0x4000
#define EN_PASSANT 0x8000
#define CASTLE     0x10000

#define FROM_SQ(x)  (((x) >>  0) & 0x7F)
#define TO_SQ(x)    (((x) >>  7) & 0x7F)
#define CAPTURED(x) (((x) >> 16) & 0xFE) 
#define PROMOTED(x) (((x) >> 24) & 0xFE)

typedef uint32_t Move;
typedef int Score;

typedef struct {
	Move move;
	Score score;
} Scored_Move;


/* TODO: maybe a dynamic array is better? */
typedef struct {
	Scored_Move scored_moves[MAX_POSITION_MOVES];
	int count;
} Move_List;

typedef struct {
	Move move;
	Castle_Permission castle_perimission;
	Board_Position en_passant;
	int fifty_move_counter;
	uint64_t position_hash;
} Undo;

typedef struct {
	Piece pieces[120];
	BitBoard pawns[3];

	Board_Position king_square[2];
	Board_Position piece_list[13][10];
	int piece_number[13];

	int material[2];
	int major_pieces[2];
	int minor_pieces[2];
	int big_pieces[2];

	Side side;
	Board_Position en_passant;
	int fifty_move_counter;

	int ply;
	int history_ply;

	Castle_Permission castle_perimission;

	uint64_t position_hash;

	Undo history[MAX_GAME_MOVES];
} Board;

void init_hash_keys(void);
void update_list_material(Board *b);
int parse_fen(const char *fen, Board *b);
void print_board(const Board *b);
void reset_board(Board *b);
int check_board(const Board *b);

int is_square_atakked(const Board *b, Board_Position sq, Side side);
void generate_all_moves(const Board *, Move_List *l);
void print_move_list(const Move_List *l);

void print_bitboard(BitBoard bb);
int pop_bit(BitBoard *bb);
int count_bits(BitBoard bb);

char *print_square(Board_Position bp, char *s);
char *print_move(Move m, char *s);
