#ifndef DEBUG
#define ASSERT(x)
#else
#include <stdio.h>
#include <stdlib.h>
#define ASSERT(x)                                          \
do {                                                       \
	if (!(x)) {                                            \
		fprintf(stderr, "Assertion `" #x "` failed at\n"); \
		fprintf(stderr, "%s:%d:0\n", __FILE__, __LINE__);  \
		exit(1);                                           \
	}                                                      \
} while (0)
#endif
